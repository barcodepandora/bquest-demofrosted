
//
//  ViewController.m
//  DemoFrosted
//
//  Created by Juan on 10/23/15.
//  Copyright (c) 2015 Juan. All rights reserved.
//
//  VC DE LA VISTA contentController
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 MUESTRA LA VISTA FROSTED
 */
- (IBAction)doIt:(id)sender {
    
    [self.frostedViewController presentMenuViewController]; // Presenta el menu.
}
@end