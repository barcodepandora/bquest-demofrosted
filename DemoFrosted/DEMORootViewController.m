//
//  DEMORootViewController.m
//  DemoFrosted
//
//  Created by Juan on 10/23/15.
//  Copyright (c) 2015 Juan. All rights reserved.
//

#import "DEMORootViewController.h"

@interface DEMORootViewController ()

@end

@implementation DEMORootViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

/*
 INSTANCIA LOS VC REQUERIDOS PARA LA VISTA FROSTED
 
 contentController ES EL StoryboardID QUE IDENTIFICA LA VISTA QUE REALIZA LA ACCION DE MOSTRAR EL MENU
 menuController ES EL StoryboardID QUE IDENTIFICA LA VISTA QUE QUE CONTIENE EL MENU
 
 AL TERMINAR, SE MUESTRA EN EL SCREEN LA VISTA contentController
 */
- (void)awakeFromNib
{
    self.contentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"contentController"];
    self.menuViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"menuController"];
}
@end
