//
//  MenuViewController.h
//  DemoFrosted
//
//  Created by Juan on 10/23/15.
//  Copyright (c) 2015 Juan. All rights reserved.
//
//  VC DE LA VISTA menuController
//  NO TIENE SUPER DEL FRAMEWORK REFrostedViewController
//

#import <UIKit/UIKit.h>
#import "REFrostedViewController.h" // IMPORTAR POR SER VC DE LA VISTA menuController

@interface MenuViewController : UIViewController

@end
