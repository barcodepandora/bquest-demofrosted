//
//  ViewController.h
//  DemoFrosted
//
//  Created by Juan on 10/23/15.
//  Copyright (c) 2015 Juan. All rights reserved.//
//
//  VC DE LA VISTA contentController
//  NO TIENE SUPER DEL FRAMEWORK REFrostedViewController
//

#import <UIKit/UIKit.h>
#import "REFrostedViewController.h" // IMPORTAR POR SER UN VC DE LA VISTA contentController

@interface ViewController : UIViewController

/*
 MUESTRA LA VISTA FROSTED
 */
- (IBAction)doIt:(id)sender;
@end

