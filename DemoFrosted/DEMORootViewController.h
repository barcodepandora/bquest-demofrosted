//
//  DEMORootViewController.h
//  DemoFrosted
//
//  Created by Juan on 10/23/15.
//  Copyright (c) 2015 Juan. All rights reserved.
//
//  VC QUE GESTIONA LAS VISTAS FROSTED
//  SUPER: REFrostedViewController
//

#import <REFrostedViewController/REFrostedViewController.h> // IMPORTAR POR SER VC QUE IMPLEMENTA awakeFromNib

@interface DEMORootViewController : REFrostedViewController

@end
